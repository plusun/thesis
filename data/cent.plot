unset log
unset label
unset x2tic
unset y2tic
# unset xtic
unset origin
#unset tics
unset arrow
unset object
#unset title
set ytic nomirror auto
set border 3 front
set ylabel "Execution Overhead (%)"
set style data histogram
set style histogram title offset 0,-1
set style fill solid border -1
set yrange [0:35]
set boxwidth 1
set key outside center top horizontal font ", 50" at 7,34 Left reverse box spacing 1
show key
set size ratio 0.35
set xtics nomirror rotate by -35 offset 0,0
set bmargin 3.5
set tmargin 0
set grid ytics lc rgb "#6d6d6d" lw 1 lt 0

plot "syncvar.dat" u 3:xtic(1) t "Centralized Controller" lc rgb "#DBDBDB", \
		'' u 4 t "Distributed Controller" lc rgb "#494949"


set term postscript eps enhanced color font 'Times-Roman,54' size 12,5.5
#set term png font 'Times-Roman,54' size 12,7
set output "cent.eps"
replot
